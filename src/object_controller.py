import getpass
import argparse
import logging
from src.object_transporter import ObjectTransporter


class TransportController(object):

    def run(self):
        logging.basicConfig(level=logging.INFO)
        argv = self.parse_args()
        if argv.clear:
            ObjectTransporter(argv.ks, argv.ed, argv.u, argv.p).run(True)
        else:
            ObjectTransporter(argv.ks, argv.ed, argv.u, argv.p).run(False)

    @staticmethod
    def parse_args():
        parser = argparse.ArgumentParser()
        parser.add_argument("-ks", help="Kibana host url for the source of objects")
        parser.add_argument("-ed", help="Elasticsearch destination url. The destination of index patterns")
        parser.add_argument("-u", help="Username")
        parser.add_argument("-clear", help="Clear data first. clear data")
        argv = parser.parse_args()
        if not argv.ks:
            argv.ks = "http://elk-swmman01.eagleinvsys.com"
        if not argv.ed:
            argv.ed = 'localhost'
        if argv.u is not None:
            argv.p = getpass.getpass('Password:')
        else:
            argv.u = 'elastic'
            argv.p = 'changeme'
        return argv