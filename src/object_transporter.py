import requests
from requests.auth import HTTPBasicAuth
import json

import logging
import logging.config
from elasticsearch import Elasticsearch


class ObjectTransporter(object):

    def __init__(self, kib_source, e_dest, auth_username=None, auth_secret=None, testing=False):
        """
        Initializes the object transporter
        :param kib_source: The source kibana index
        :param e_dest: The elasticsearch destination
        :param auth_username: Your username
        :param auth_secret: Your secret
        """

        logging.config.dictConfig({
            'version': 1,
            'disable_existing_loggers': True
        })
        logger = logging.getLogger()
        logger.handlers = []
        handler = logging.StreamHandler()
        handler.setLevel(logging.INFO)
        formatter = logging.Formatter("[%(levelname)s] %(message)s")
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        logging.basicConfig(level=logging.INFO)

        self.kib_source = kib_source
        self.e_dest = e_dest
        self.uname = auth_username
        self.usect = auth_secret
        self.testing = testing
        if auth_username or auth_secret is not None:
            try:
                self.client = Elasticsearch([e_dest], http_auth=(auth_username, auth_secret))
            except:
                raise ValueError("Invalid Host or Authentication")
        else:
            self.client = Elasticsearch([e_dest])

    def run(self, clear=False):
        """
        Run the Saved Objects Transfer
        :return:
        """
        if clear:
            self.clear_objects(self.testing)
        objects = self.get_objects()
        self.post_objects(objects)

    def get_objects(self, host=None):
        """
        Retrieve the objects
        :return: The saved objects associated with the given kibana url
        """
        if host is None:
            host = self.kib_source
        try:
            url = host + ":5601/es_admin/.kibana/_search?size=1000"
            response = requests.get(url=url, auth=HTTPBasicAuth(self.uname, self.usect))
            saved_objects = json.loads(response.text)['hits']['hits']
            if response.status_code != 200:
                logging.error(response.headers)
                exit(1)
            else:
                logging.info("Successfully fetched objects")
                return saved_objects
        except:
            logging.error("Failed to get objects")
            exit(1)

    def post_objects(self, objects):
        """
        Post the objects to the kibana index within the given elasticsearch destination
        :param objects: The objects to post
        :return: Nothing
        """
        logging.info("Posting Data")
        for sav_object in objects:
            id = sav_object["_id"]
            try:
                self.client.index(index='.kibana', doc_type=sav_object["_type"], id=id, body=sav_object["_source"])
            except:
                logging.error(sav_object["_id"] + " could not be posted.")

    def clear_objects(self, testing=False):
        """
        Clears all Kibana data
        :return: Nothing
        """
        if testing:
            logging.info("Deleting Data")
            for item in self.get_objects('http://' + self.e_dest):
                self.client.delete(index=".kibana", doc_type=item["_type"], id=item["_id"])
        else:
            logging.warning("This will erase all current Kibana data in " + self.e_dest + ", are you sure you want to do this?")
            answer = input("(y/n)\n").lower()
            while answer != 'y' and answer != 'no':
                logging.info("Invalid Input")
                answer = input("(y/n)\n")

            if answer == "y":
                logging.info("Deleting Data")
                for item in self.get_objects('http://' + self.e_dest):
                    self.client.delete(index=".kibana", doc_type=item["_type"], id=item["_id"])
            elif answer == 'no':
                exit(0)
