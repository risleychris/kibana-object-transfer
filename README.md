# Elasticsearch Saved Object Transporter
Given a Kibana host URL and an Elasticsearch host url, this program will take the saved objects
associated with the source Kibana (dashboard, visualization, and search objects) and import the objects into
the kibana index associated with the given elasticsearch cluster. 


### Running the Saved Objects Transfer

    python3 kibana-object-transfer.py -u <xid> -ks <kibana source host> -ed <elasticsearch destination>
   
### Clearing saved object data before transferring

    python3 kibana-object-transfer.py -u <xid> -ks <kibana source host> -ed <elasticsearch destination> -clear all
    
### Testing

Enter the following via command line

    pytest


**To run the test, you must have a local elasticsearch instance up and running**

### Setting up elasticsearch

1. Install docker
2. Type the following in command line
    
    
    docker-compose -f ./path-to-build.yml/build.yml -p logging up
