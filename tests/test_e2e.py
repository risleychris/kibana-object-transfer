import unittest
from src.object_transporter import ObjectTransporter


class E2ETests(unittest.TestCase):

    def get_transporter(self):
        return ObjectTransporter('http://elk-swmman01.eagleinvsys.com', 'localhost', 'elastic', 'changeme')

    def test_run(self):
        success = False
        try:
            ec = self.get_transporter()
            ec.run()
            objects = ec.get_objects()
            dest_objects = ec.get_objects('http://localhost')
            self.assertTrue(len(objects) == len(dest_objects))
            success = True
        except:
            pass
        self.assertTrue(success)


if __name__ == '__main__':
    unittest.main()