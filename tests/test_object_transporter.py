from src.object_transporter import ObjectTransporter
from unittest import TestCase
import unittest

class TestObjectTransporter(TestCase):

    def get_transporter(self):
        return ObjectTransporter('http://elk-swmman01.eagleinvsys.com', 'localhost', 'elastic', 'changeme')

    def test_get_objects(self):
        ec = self.get_transporter()
        objects = ec.get_objects()
        size = len(objects)
        self.assertTrue(size >= 195)

    def test_post_objects(self):
        ec = self.get_transporter()
        objects = ec.get_objects()
        ec.post_objects(objects)
        dest_objects = ec.get_objects('http://localhost')
        self.assertTrue(len(objects) == len(dest_objects))


if __name__ == '__main__':
    unittest.main()