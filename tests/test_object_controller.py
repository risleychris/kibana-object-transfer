from src.object_controller import TransportController
from unittest import TestCase
import unittest
import sys


class TestObjectController(TestCase):

    def test_kibana_source(self):
        sys.argv = ['kibana-object-transfer.py', '-ks', 'test-host']
        argv = TransportController().parse_args()
        self.assertEquals(argv.ks, 'test-host')

    def test_elastic_dest(self):
        sys.argv = ['kibana-object-transfer.py', '-ed', 'test-host-dest']
        argv = TransportController().parse_args()
        self.assertEquals(argv.ed, 'test-host-dest')

    def test_kibana_source_default(self):
        sys.argv = ['kibana-object-transfer.py', '-ks', 'test-host']
        argv = TransportController().parse_args()
        self.assertEquals(argv.ed, 'localhost')

    def test_elastic_dest_default(self):
        sys.argv = ['kibana-object-transfer.py', '-ed', 'test-host-dest']
        argv = TransportController().parse_args()
        self.assertEquals(argv.ks, 'http://elk-swmman01.eagleinvsys.com')

    def test_default_username(self):
        sys.argv = ['kibana-object-transfer.py']
        argv = TransportController().parse_args()
        self.assertEquals(argv.u, 'elastic')

    def test_default_password(self):
        sys.argv = ['kibana-object-transfer.py']
        argv = TransportController().parse_args()
        self.assertEquals(argv.p, 'changeme')


if __name__ == '__main__':
    unittest.main()